[BarGuildでBMSやる会5](http://tweetvite.com/event/bms_BarGuild5) プレイ曲まとめ（暫定）

前半

1. Amusing Night Party / yukitani
2. Aerialwalker / 天游
3. Afterimage / KHTP
4. Summertime(2013 mix) / Lime
5. 20140705 / Extrose [持込] *1
6. seabird (DOG-176 RMX) / 鳥海改(b-UMB) [持込] *2
7. Absurd Gaff / siromaru
8. landscape / tcheb / ヨダス田中 [持込]
9. Bergnebel / Airylic
10. Stella Symphonia / NS-Factory × cislv
11. Orcinus / Zicra
12. cold planet / knot
13. 織姫 / DJ TAKUAN 及び 佐倉アヤキ
14. squartatrice / 美樹さやか vs. 美樹さやか (fw. 美樹さやか) BGA : 美樹さやか
15. conflict / siromaru + cranky
16. PLASTIC GIRLS (ft. Seorryang) / wigen
17. rainy beauty / Rabbit Bat
18. Neck Breaker / Nitrix
19. B.B.K.K.B.K.K. / nora2r / 出前
20. Big Ben / kireji
21. Snow "GAP mix" / cranky
22. Highleg Mermaid / MUSIC:片桐早苗 BGA:輿水幸子
23. UNIGURI / SHiNKA
24. the sorrow of the islands / narve
25. Kaiden Aura / yukitani
26. Infinite Galaxy / NS-Factory × deadblue238 × hi-low
27. Pure Boys / luzebzk(from 300club)
28. Poppin' Shower / P*Light
29. 101 Ways / ナタネ
30. T.Y.H.Y. / Personative
31. 虚空グラデーション / fmy. × TrioStaR × Lylica* × b / cislv × Mentalstock
32. 宇宙のMI'Sキッチン (Ras Remix) / ねこみりん feat.みゆ Remixed by Ras
33. conflict / siromaru + cranky
34. R176 / cranky
35. Absurd Gaff / siromaru
36. しろまるの主張 -PUBLIC EDITION- / DJ AMANO

後半

1. Poppin' Shower / P*Light
2. SUNRISE BEACH -暁星の雫- / shimizushi
3. 天風 / うまるつふり
4. Lumos (BMS Edit) / L.K.
5. Dandelion Sparkle!! / p_d
6. 記憶 / syzfonics
7. We are the xxxx / ikaruga_nex feat.Salita
8. symbolic gear / siromaru
9. 20140705 / Extrose
10. BlackFeather / 重巡洋艦 羽黒
11. Destined Marionette / beatMARIO
12. One step toward you / 立花あすみ×空見沢佳奈
13. Evanescent / LeaF / Optie
14. L.D. -2007 HC Ver- / Saka-ROW組 feat. HINA
15. Cinderella Story / adumac
16. Gamegame (bms edit) / s-don / 篠乃ちよ
17. 7 colors\* / Ym1024 feat. lamie\*
18. Magical Love Words / 源屋feat.ユスラ
19. 恋愛シンドローム / 佐久間まゆ / 星輝子
20. oragegirl sentiment / recognize m.
21. cold planet (Oceanfront City Remix) / knot / Yamajet
22. yksinaisyys / Le Dos-on
23. The Island of Albatross / anubasu-anubasu
24. JULIAN "for B.O.F. 2009" / Queen P.A.L.
25. My DIMENSIONS OF THE FOUR / LU VS 8 to 7
26. Party 4U / cranky

註

* 1) 内容はRyutarostep（兵庫県議号泣会見ネタ）と幾つかのBMS（B.B.K.K.B.K.K., FRANTIC☆ARCADE, conflict, DJおっさん）の一部分を繋げたもの。（[本人のツイート](https://twitter.com/extrose_xinei/status/485282560411332608)）
* 2) リミックス名は[Bluvelさんの会場内からのツイート](https://twitter.com/Bluvel/status/485281040974364674)を元に付加。