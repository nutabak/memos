# [BarGuildでBMSやる会4](http://tweetvite.com/event/bms_BarGuild4)プレイリストメモ

[現地からプレイされた曲を一部ツイートしてくれた方の情報](https://yukar.in/note/ckFh95)を基に修正。抜けや誤りがあるかもしれないが、これにて一区切り。

クイズやKOFについては割愛。

## BMS FreePlay #1
* leve(remix) / 8 to 7 remixed by LU
* colorbar /roop
* Colored / yamajet
* furioso melodia / gmtn. 
* u gotta party / 天音
* Messier 333 / sun3
* to Firmament / 赤城
* Seadbird / 鳥海改
* LUV TO ME 2011 / カラフル・サウンズ・ポート
* SUNRISE BEACH -暁星の雫- / shimizushi
* 悪★徳★蛸 ( Original Mix ) / Kak-Ta (original:myu)
* binary rendezvous / yamajet BGA:tutidama
* santuario / Signal Love
* Drive 100 / ZUN remixed by daph [註:東方音弾遊戯6登録に先行して公開]
* Guiding Star / cittan* feat. YUNA
* HARD RUNNING IN THE air / DJ SIMON [註:with SECRET-B?]
* 図書室のエルザ / 橘花音 かなえゆめ composed by nmk
* T.Y.H.Y. / Personative
* Gothic System / TCN180 (DJ TECHNORCH)
* ヤマジェット☆コースターガール / Yamajet☆
* 少女が見たロケテの激譜面 / VAK underground
* Foresta / Phorni
* 人間の盾 / 赤蓮舞踊師嵐堂
* Highleg Mermaid / MUSIC:片桐早苗 / BGA:輿水幸子
* Kaiden Aura / yukitani
* ミリオンスロット
* Apocalypse / yugos
* We are the xxxx / ikaruga_nex feat. Salita
* lucu perjamuan / dge-nesis
* FRANTIC☆ARCADE / DJ PrimeMIX

## BMS FreePlay #2
* UK道中ハーコー万丈 / 猫足零本 feat. ひめりんご
* ピアノ協奏曲第-1番 "蠍ヴィ"
* STAGER / Ras
* sevenscape / xarva
* Ponytail / Est
* UNIGURI / SHiNKA
* BLOOD BLOOD BLOOD / 新装姉妹
* dolorosa melodia / がむつ氏 + けえ氏 (a.k.a. witch's slave)
* Gamegame / s-don
* TRAGIC STOMPING MACHINE / asceticaudio.
* Reality / SHK
* Miracle? / NISR
* りりくろ! / ねここうもり
* DRAGONLADY / Nankumo
* Right Back / ag
* Fruit collection / Bit's
* 紅 / YOUDIE
* ZEUS+1 / ルゼ
* SkyGazer / 空見沢佳奈
* Citron / Saldy
* ねぇ？メイドさんはみんな忠実だと思う？ / かおみりん feat. こもも×みゆ
* MESHER DIMENSION / REX
* Altostratus / syatten
* 妹軍団襲来 / Pinky
* MoonGlow / okari vs puru
* Meteor Lights / s-don
* Day of separation / rider
* conflict / siromaru + cranky
* drivin' IC / 908
